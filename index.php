<?php

require_once 'vendor/autoload.php';

use Acme\BaseProduct;
use Acme\Store;


// 15 yo customer
echo "15 y.o. customer <br>";

$customer_age = 15;
$budget       = 300;

$store    = new Store($customer_age);
$products = $store->getProducts();

for ($i = 0; count($store->getCart()) < 6; $i++) {
    $product = $store->getRandomProduct();
    $store->addToCart($product);

    if ($budget - $product->getPrice() <= 0 && $i == 5) {
        echo "Refusing product because it is too expensive <br>";
        break;
    }

    $budget = $budget - $product->getPrice();
}

echo "Cart: Product - Original Price - Computed Price<br>";
foreach ($store->getCart() as $i => $item) {
    echo "Product " . ($i + 1) . " = ". $item->getOriginalPrice() . " = "  . $item->getPrice() . "<br>";
}

echo "-------------------- <br>";
echo "Total: " . $store->getTotal() . "<br>";
echo "Discounted: ". $store->getDiscount($store->getTotal()) . "<br>";
echo "Final Total " . $store->getFinalTotal();



echo "<br><br><br>";
// 51 yo customer
echo "51 y.o. customer <br>";

$customer_age = 51;

$store    = new Store($customer_age);
$products = $store->getProducts();
$total = 0;

while($total < 300) {
    $product = $store->getRandomProduct();
    $store->addToCart($product);

    $total = $total + $product->getPrice();
}

echo "Cart: Product - Original Price - Computed Price<br>";
foreach ($store->getCart() as $i => $item) {
    echo "Product " . ($i + 1) . " = ". $item->getOriginalPrice() . " = "  . $item->getPrice() . "<br>";
}

echo "-------------------- <br>";
echo "Total: " . $store->getTotal() . "<br>";
echo "Discounted: ". $store->getDiscount($store->getTotal()) . "<br>";
echo "Final Total " . $store->getFinalTotal();

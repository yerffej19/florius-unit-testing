<?php

namespace Acme;

class Store
{

    const MAX_STORE_ITEMS    = 300;
    const MAX_UNDER_18_ITEMS = 35;
    const MAX_ABOVE_51_ITEMS = 92;

    const DISCOUNTABLE_TOTAL = 300;
    const DISCOUNT_RATE = 0.1;

    protected $customer_age;
    protected $products = [];
    protected $cart = [];
    protected $total;

    public function __construct($customer_age)
    {
        if ($customer_age) {
            $this->setCustomerAge($customer_age);
            $this->generateProducts();
        }

    }

    public function generateProducts()
    {
        $age       = $this->getCustomerAge();
        $age_group = $this->getAgeGroup();

        if ($age < 18) {
            for ($i = 0; $i < self::MAX_UNDER_18_ITEMS; $i++) {
                $this->pushProduct(new Below18Product($age_group));
            }
        }

        if ($age > 51) {
            for ($i = 0; $i < self::MAX_ABOVE_51_ITEMS; $i++) {
                $this->pushProduct(new Above50Product($age_group));
            }
        }

        for ($i = 0; $i < (self::MAX_STORE_ITEMS - (self::MAX_UNDER_18_ITEMS + self::MAX_ABOVE_51_ITEMS)); $i++) {
            $this->pushProduct(new DefaultProduct($age_group));
        }
    }

    protected function pushProduct($product)
    {
        array_push($this->products, $product);
    }

    public function addToCart($product)
    {
        array_push($this->cart, $product);
    }

    public function getRandomProduct()
    {
        $products = $this->getProducts();
        $index = rand(0, count($products) - 1);

        return $products[$index];
    }

    public function getAgeGroup()
    {
        $age = $this->getCustomerAge();

        if ($age < 18)
            return AgeGroup::AGE_GROUP_BELOW_18;
        else if ($age >= 27 && $age <= 50)
            return AgeGroup::AGE_GROUP_27_TO_50;
        else if ($age > 50)
            return AgeGroup::AGE_GROUP_ABOVE_50;

        return AgeGroup::AGE_GROUP_DEFAULT;
    }

    public function getTotal()
    {
        $cart = $this->getCart();
        $total = 0;

        /** @var BaseProduct $item */
        foreach ($cart as $item) {
            $total = $total + $item->getPrice();
        }

        return $total;
    }

    public function getDiscount($total)
    {
        return $total > self::DISCOUNTABLE_TOTAL ? ($total * self::DISCOUNT_RATE) : 0;
    }

    public function getFinalTotal()
    {
        return $this->getTotal() - $this->getDiscount($this->getTotal());
    }

    /**
     * @return mixed
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @return mixed
     */
    public function getCustomerAge()
    {
        return $this->customer_age;
    }

    /**
     * @param mixed $customer_age
     */
    public function setCustomerAge($customer_age)
    {
        $this->customer_age = $customer_age;
    }

    /**
     * @return array
     */
    public function getCart()
    {
        return $this->cart;
    }

    /**
     * @param array $cart
     */
    public function setCart($cart)
    {
        $this->cart = $cart;
    }


}
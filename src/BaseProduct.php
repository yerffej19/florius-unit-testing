<?php
namespace Acme;

class BaseProduct {

    protected $price;
    protected $customer_age_group;

    public function __construct($customer_age_group = null)
    {
        if ($customer_age_group) {
            $this->setCustomerAgeGroup($customer_age_group);
        }

        $this->setPrice(rand(10, 50));
    }

    /**
     * @return mixed
     */
    public function getCustomerAgeGroup()
    {
        return $this->customer_age_group;
    }

    /**
     * @param mixed $customer_age_group
     */
    public function setCustomerAgeGroup($customer_age_group)
    {
        $this->customer_age_group = $customer_age_group;
    }

    public function getOriginalPrice()
    {
        return $this->price;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }
    
}


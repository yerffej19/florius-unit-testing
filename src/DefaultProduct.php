<?php

namespace Acme;

class DefaultProduct extends BaseProduct
{

    const PREMIUM_RATE = 0.05;
    const DISCOUNT_RATE_AGE_GROUP_27_TO_50 = 0.05;
    CONST DISCOUNT_RATE_ABOVE_50 = 0.1;

    public function getPrice()
    {
        $price              = parent::getPrice();
        $customer_age_group = $this->getCustomerAgeGroup();

        switch ($customer_age_group) {
            case AgeGroup::AGE_GROUP_BELOW_18:
                $price = $price + (self::PREMIUM_RATE * $price);
                break;
            case AgeGroup::AGE_GROUP_27_TO_50:
                $price = $price - (self::DISCOUNT_RATE_AGE_GROUP_27_TO_50 * $price);
                break;
            case AgeGroup::AGE_GROUP_ABOVE_50:
                $price = $price - (self::DISCOUNT_RATE_ABOVE_50 * $price);
        }

        return $price;
    }
}
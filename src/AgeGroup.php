<?php
namespace Acme;

class AgeGroup {

    const AGE_GROUP_BELOW_18 = 'Below 18';
    const AGE_GROUP_ABOVE_50 = 'Above 50';
    const AGE_GROUP_27_TO_50 = '27 to 50';
    const AGE_GROUP_DEFAULT  = 'Default';

}